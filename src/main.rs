use std::fs;
use std::fs::File;
use std::path::Path;
use std::io::{BufRead, BufReader};

use std::collections::HashMap;

extern crate clap;
extern crate hex;
extern crate log4rs;
extern crate sha2;
extern crate walkdir;

use clap::{Arg, App};
use log4rs::append::console::ConsoleAppender;
use log4rs::config::{Appender, Config, Root};
use log::LevelFilter;
use log::{error,info};
use regex::Regex;
use sha2::{Sha512, Digest};
use walkdir::WalkDir;

const INDEX_FILE: &str = "index.sha512sum";

fn main() {
    //log4rs::init_file("log4rs.yml", Default::default()).unwrap();
    //

    let stdout = ConsoleAppender::builder().build();

    let config = Config::builder()
        .appender(Appender::builder().build("stdout", Box::new(stdout)))
        .build(Root::builder().appender("stdout").build(LevelFilter::Info))
        .unwrap();

    match log4rs::init_config(config) {
        Ok(_) => (),
        Err(e) =>  {
            error!("Failed to initialize logger ue to '{}'", e);
            std::process::exit(1);
        }
    }

    let matches = App::new("checksummer")
        .version("1.0")
        .author("Manu <mrkumar9001@gmail.com>")
        .about("Directory checksummer with incremental updates.")
        .arg(Arg::with_name("command")
            .help("Which command to use.")
            .takes_value(false)
            .possible_values(&["c", "check", "u", "update"])
            .required(true)
        )
        .arg(Arg::with_name("directory")
            .help("Which directory to use (optional).")
            .takes_value(false)
            .required(false)
        )
        .get_matches();

    let command = match matches.value_of("command") {
        Some(cmd) => cmd,
        None      => {
            error!("No value for 'command'.");
            std::process::exit(1)
        }
    };

    let directory_str = matches.value_of("directory").unwrap_or(".");
    let directory = Path::new(directory_str);
    if !directory.is_dir() {
        error!("Directory '{}' is not valid.", directory_str);
        std::process::exit(1);
    }
    info!("Directory is '{}'.", directory_str);

    match command {
        "c" | "check"  => operation_check(directory),
        "u" | "update" => operation_update(directory),
        other          => {
            error!("What, we should have this value: '{}'", other);
            std::process::exit(1)
        }
    }
}

fn operation_check(directory: &Path) {
    info!("Doing check.");

    let checksum = directory.join(INDEX_FILE);
    if !checksum.is_file() {
        error!("{} does not exist in directory.", checksum.display());
        std::process::exit(1);
    }

    // One map to keep track of what's actually on the filesystem, and another
    // to check what's in the 'index.sha512sum' file.
    //let mut actual_checksums = HashMap::new();
    let mut expect_checksums = HashMap::new();

    let re = match
        Regex::new(r"^(?P<checksum>[0-9a-f]{128})\s+(?P<filename>.*)$") {
            Ok(re) => re,
            Err(e) => {
                error!("Failed to create regex expression due to '{}'", e);
                std::process::exit(1);
            }
        };
    let file = File::open(checksum).unwrap();
    let reader = BufReader::new(file);
    let mut line_no: u32 = 1;
    for line in reader.lines() {
        // Get line string.
        let linestr = match line {
            Ok(linestr) => linestr,
            Err(e)  => {
                error!("Failed to read line {}: '{}'.", line_no, e);
                continue;
            }
        };

        // Capture text with regex and ensure.
        let captures = match re.captures(&linestr) {
            Some(cap) => cap,
            None  => {
                error!("Line number {} is malformed: '{}'!", line_no, linestr);
                continue;
            }
        };
        let checksum = match captures.name("checksum") {
            Some(val) => val.as_str(),
            None      => {
                error!("Failed to get checksum on line {}: '{}'!", line_no,
                    linestr);
                continue
            }
        };
        let filename = match captures.name("filename") {
            Some(val) => val.as_str(),
            None      => {
                error!("Failed to get filename on line {}: '{}'!", line_no,
                    linestr);
                continue;
            }
        };
        assert_eq!(
            expect_checksums.insert(filename.to_string(), checksum.to_string()),
            None);

        // Increment since we'll be on the next line.
        line_no += 1;
    }
}

/// Print the key value entries in the map.
fn print_checksum_map(checksum_map: HashMap<String, String>) {
    for (key, val) in checksum_map.iter() {
        info!(" * {} -> {}", key, val);
    }
}

/// Returns sha512 hash of a file.
fn sha512_hash(path: &Path) -> String {
    let mut hasher = Sha512::new();

    match fs::read(path) {
        Ok(data) => hasher.update(data),
        Err(e) => {
            error!("Failed to read file '{}' due to '{}'.",
                path.display(), e)
        }
    }
    let result = hasher.finalize();
    return hex::encode(result);
}

fn checksum_dir(directory: &Path) -> HashMap<String, String> {
    info!("Walking directory '{}'.", match directory.to_str() {
        Some(val) => val,
        None => "<Path has invalid UTF-8>",
    });

    let mut actual_checksums: HashMap<String, String> = HashMap::new();

    for entry in WalkDir::new(directory) {
        match entry {
            // We don't care about index file.
            Ok(entry) => {
                let path = entry.path();
                // Skip processing about index file.
                match path.file_name() {
                    Some(name) => if name == INDEX_FILE { continue; }
                    None       => {},
                }
                if path.is_file() {
                    let path_str = path.to_str().unwrap().to_string();
                    actual_checksums.insert(path_str, sha512_hash(path));
                }
            }
            Err(e)    => info!("Error {}.", e),
        }
    }

    return actual_checksums;
}

fn operation_update(directory: &Path) {
    info!("Doing update.");
    print_checksum_map(checksum_dir(directory));
}
